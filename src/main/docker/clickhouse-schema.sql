CREATE TABLE "shop" ("id" integer PRIMARY KEY,"name" varchar(255) NOT NULL,"address" text NOT NULL)  ENGINE = MergeTree();
CREATE TABLE "product" ("id" integer PRIMARY KEY,"name" varchar(255) NOT NULL,"price" numeric(10, 2) NOT NULL)  ENGINE = MergeTree();
CREATE TABLE "purchase_details" ("id" integer PRIMARY KEY,"purchase_id" integer,"product_id" integer,"amount" integer,"price" numeric(10, 2) NOT NULL)  ENGINE = MergeTree();
CREATE TABLE "purchases" ("id" integer PRIMARY KEY,"shop_id" integer,"date" timestamp)  ENGINE = MergeTree();
