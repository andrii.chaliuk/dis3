CREATE TABLE "shop" ("id" integer PRIMARY KEY,"name" varchar(255) NOT NULL,"address" text NOT NULL);

CREATE TABLE "product" ("id" integer PRIMARY KEY,"name" varchar(255) NOT NULL,"price" numeric(10, 2) NOT NULL);

CREATE TABLE "purchase_details" ("id" integer PRIMARY KEY,"purchase_id" integer,"product_id" integer,"amount" integer,"price" numeric(10, 2) NOT NULL);

CREATE TABLE "purchases" ("id" integer PRIMARY KEY,"shop_id" integer,"date" timestamp);

ALTER TABLE "purchases" ADD FOREIGN KEY ("shop_id") REFERENCES "shop" ("id");

ALTER TABLE "purchase_details" ADD FOREIGN KEY ("product_id") REFERENCES "product" ("id");

ALTER TABLE "purchase_details" ADD FOREIGN KEY ("purchase_id") REFERENCES "purchases" ("id");
