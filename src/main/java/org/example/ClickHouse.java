package org.example;

import com.clickhouse.jdbc.ClickHouseDataSource;

import java.io.*;
import java.sql.*;
import java.sql.Date;
import java.util.*;

public class ClickHouse {

    private static final String resPath = "src/main/results/clickhouse/";
    private static final String dataFile = "src/main/docker/clickhouse-init.sql";
    private static final String schemaFile = "src/main/docker/clickhouse-schema.sql";
    private static final Random random = new Random();

    public static void addData(Connection connection){
        try {
            File myObj = new File(schemaFile);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                // System.out.println(data);
                PreparedStatement preparedStatement = connection.prepareStatement(data);
                ResultSet resultSet = preparedStatement.executeQuery();
            }
            myReader.close();
        } catch (FileNotFoundException | SQLException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        try {
            File myObj = new File(dataFile);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                // System.out.println(data);
                PreparedStatement preparedStatement = connection.prepareStatement(data);
                ResultSet resultSet = preparedStatement.executeQuery();
            }
            myReader.close();
        } catch (FileNotFoundException | SQLException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    // All sum
    public static void task1(Connection connection, String fileName) throws SQLException, IOException {
        String selectQuery =
                "SELECT SUM(amount) AS AmountSum"
                        + " FROM purchase_details";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);

        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (resultSet.next()) {
            int sum = resultSet.getInt("AmountSum");
            String res = "product sum: " + sum;
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }

        resultSet.close();
        preparedStatement.close();
    }

    // By product name
    public static void task1_2(Connection connection, String fileName, String productName) throws SQLException, IOException {
        String selectQuery =
                "SELECT SUM(pd.amount) AS AmountSum" +
                        " FROM purchase_details pd INNER JOIN product p ON pd.product_id = p.id WHERE p.name = ?";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setString(1, productName);

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        while (resultSet.next()) {
            int sum = resultSet.getInt("AmountSum");
            String res = "product sum: " + sum;
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    // For every product
    public static void task1_3(Connection connection, String fileName) throws SQLException, IOException {
        String selectQuery =
                "SELECT p.name AS ProductName, SUM(pd.amount) AS AmountSum" +
                        " FROM purchase_details pd INNER JOIN product p ON pd.product_id = p.id" +
                        " GROUP BY p.name " +
                        "ORDER BY AmountSum DESC ";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        while (resultSet.next()) {
            String productName = resultSet.getString("ProductName");
            int sum = resultSet.getInt("AmountSum");
            String res = "{\n\tproduct: " + productName + "\n\tsum: " + sum + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task2(Connection connection, String fileName) throws SQLException, IOException {
        String selectQuery =
                "SELECT SUM(price) AS PriceSum"
                        + " FROM purchase_details";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (resultSet.next()) {
            String priceSum = resultSet.getString("PriceSum");
            String res = "{\n\tprice sum: " + priceSum + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task2_1(Connection connection, String fileName) throws SQLException, IOException {
        String selectQuery =
                "SELECT P.name AS ProductName, SUM(PD.amount * P.price) AS Cost\n" +
                        "FROM purchase_details PD " +
                        "INNER JOIN product P ON PD.product_id = P.id\n" +
                        "INNER JOIN purchases PU ON PD.purchase_id = PU.id\n" +
                        "GROUP BY P.name\n" +
                        "ORDER BY Cost DESC \n";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (resultSet.next()) {
            String productName = resultSet.getString("ProductName");
            String cost = resultSet.getString("Cost");
            String res = "{\n\tproduct: " + productName + "\n\tcost: " + cost + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task3(Connection connection, String fileName, Timestamp stateDate, Timestamp endDate) throws SQLException, IOException {
        String selectQuery =
                "SELECT SUM(price) AS PriceSum\n" +
                        "FROM purchase_details p \n" +
                        "INNER JOIN purchases pu ON pu.id = p.purchase_id\n" +
                        "WHERE pu.date >= ? AND pu.date <= ? ";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setTimestamp(1, stateDate);
        preparedStatement.setTimestamp(2, endDate);
        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (resultSet.next()) {
            String priceSum = resultSet.getString("PriceSum");
            String res = "{\n\tprice sum " + stateDate + "-" + endDate +" : " + priceSum + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task3_1(Connection connection, String fileName,
                               Timestamp stateDate, Timestamp endDate) throws SQLException, IOException {
        String selectQuery =
                "SELECT P.name AS ProductName, SUM(PD.amount * P.price) AS Cost\n" +
                        "FROM purchase_details PD " +
                        "INNER JOIN product P ON PD.product_id = P.id\n" +
                        "INNER JOIN purchases PU On PU.id = PD.purchase_id\n" +
                        "WHERE PU.date >= ? AND PU.date <= ? " +
                        "GROUP BY P.name\n " +
                        "ORDER BY Cost DESC ";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setTimestamp(1, stateDate);
        preparedStatement.setTimestamp(2, endDate);
        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (resultSet.next()) {
            String productName = resultSet.getString("ProductName");
            String priceSum = resultSet.getString("Cost");
            String res = "{\n\tprice sum for " + productName + " " + stateDate + "-" + endDate + " : " + priceSum + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task4(Connection connection, String fileName, String productName,
                             String shopName, Timestamp startDate, Timestamp endDate) throws SQLException, IOException {
        String selectQuery =
                "SELECT SUM(PD.amount) AS SumAmount\n" +
                        "FROM purchases PU\n" +
                        "INNER JOIN purchase_details PD ON PU.id = PD.purchase_id\n" +
                        "INNER JOIN product P ON PD.product_id = P.id\n" +
                        "INNER JOIN shop S ON PU.shop_id = S.id\n" +
                        "WHERE P.name = ? AND S.name = ? AND PU.date >=? AND PU.date <= ?\n";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setString(1, productName);
        preparedStatement.setString(2, shopName);
        preparedStatement.setTimestamp(3, startDate);
        preparedStatement.setTimestamp(4, endDate);

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        while (resultSet.next()) {
            String amount = resultSet.getString("SumAmount");
            String res = "{\n\tAmount of " + productName + " in " + shopName + " " + startDate +
                    "-" + endDate + ": " + amount + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task5(Connection connection, String fileName, String productName,
                             Timestamp startDate, Timestamp endDate) throws SQLException, IOException {
        String selectQuery =
                "SELECT SUM(PD.amount) AS SumAmount\n" +
                        "FROM purchases PU\n" +
                        "INNER JOIN purchase_details PD ON PU.id = PD.purchase_id\n" +
                        "INNER JOIN product P ON PD.product_id = P.id\n" +
                        "INNER JOIN shop S ON PU.shop_id = S.id\n" +
                        "WHERE P.name = ? AND PU.date >=? AND PU.date <= ?\n";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setString(1, productName);
        preparedStatement.setTimestamp(2, startDate);
        preparedStatement.setTimestamp(3, endDate);

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        while (resultSet.next()) {
            String amount = resultSet.getString("SumAmount");
            String res = "{\n\tAmount of " + productName + " " + startDate +
                    "-" + endDate + ": " + amount + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task5_1(Connection connection, String fileName, String productName,
                               Timestamp startDate, Timestamp endDate) throws SQLException, IOException {
        String selectQuery =
                "SELECT S.id AS StoreId, S.name AS ShopName, SUM(PD.amount) AS SumAmount\n" +
                        "FROM purchases PU\n" +
                        "INNER JOIN purchase_details PD ON PU.id = PD.purchase_id\n" +
                        "INNER JOIN product P ON PD.product_id = P.id\n" +
                        "INNER JOIN shop S ON PU.shop_id = S.id\n" +
                        "WHERE P.name = ? AND PU.date >=? AND PU.date <= ?\n" +
                        "GROUP BY S.id, S.name";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setString(1, productName);
        preparedStatement.setTimestamp(2, startDate);
        preparedStatement.setTimestamp(3, endDate);

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        while (resultSet.next()) {
            String amount = resultSet.getString("SumAmount");
            String shopId = resultSet.getString("StoreId");
            String shopName = resultSet.getString("ShopName");
            String res = "{\n\tShopId: " + shopId +
                    "\n\tshop name: " +  shopName +
                    "\n\tproduct name:" + productName +
                    "\n\tstartDate: " + startDate +
                    "\n\tendDate: " + endDate +
                    "\n\tamount: " + amount + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }


    public static void task6(Connection connection, String fileName,
                             Timestamp startDate, Timestamp endDate) throws SQLException, IOException {
        String selectQuery =
                "SELECT S.name AS ShopName, SUM(PD.amount * P.price) AS Revenue\n" +
                        "FROM purchases PU\n" +
                        "INNER JOIN purchase_details PD ON PU.id = PD.purchase_id\n" +
                        "INNER JOIN shop S ON PU.shop_id = S.id\n" +
                        "INNER JOIN product P ON PD.product_id = P.id\n" +
                        "WHERE PU.date >= ? AND PU.date <= ?\n" +
                        "GROUP BY S.name\n" +
                        "ORDER BY Revenue DESC ";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setTimestamp(1, startDate);
        preparedStatement.setTimestamp(2, endDate);

        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (resultSet.next()) {
            String revenue = resultSet.getString("Revenue");
            String shopName = resultSet.getString("ShopName");
            String res = "{\n\tshop name: " + shopName + ",\n\trevenue: " + revenue +
                    "\n\tperiod: " + startDate + "-" + endDate + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task7(Connection connection, String fileName,
                             Timestamp startDate, Timestamp endDate) throws SQLException, IOException {
        String selectQuery =
                "SELECT P1.name AS ProductName1, P2.name AS ProductName2, Count(*) AS Frequency\n" +
                        "FROM purchase_details PD1\n" +
                        "INNER JOIN purchase_details PD2 ON PD1.purchase_id = PD2.purchase_id \n" +
                        "INNER JOIN product P1 ON PD1.product_id = P1.id\n" +
                        "INNER JOIN product P2 ON PD2.product_id = P2.id\n" +
                        "INNER JOIN purchases PU ON PU.id = PD1.purchase_id\n" +
                        "WHERE PU.date >= ? AND PU.date <= ? AND PD1.product_id < PD2.product_id\n" +
                        "GROUP BY P1.name, P2.name\n" +
                        "ORDER BY Frequency DESC\n" +
                        "LIMIT 10";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setTimestamp(1, startDate);
        preparedStatement.setTimestamp(2, endDate);

        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (resultSet.next()) {
            String frequency = resultSet.getString("Frequency");
            String productName1 = resultSet.getString("ProductName1");
            String productName2 = resultSet.getString("ProductName2");
            String res = "{\n\tproduct1: " + productName1 +
                    ",\n\tproduct2: " + productName2 +
                    ",\n\tfrequency: " + frequency +
                    ",\n\tperiod: " + startDate + "-" + endDate + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task8(Connection connection, String fileName,
                             Timestamp startDate, Timestamp endDate) throws SQLException, IOException {
        String selectQuery =
                "SELECT P1.name AS ProductName1," +
                        " P2.name AS ProductName2," +
                        " P3.name AS ProductName3," +
                        " Count(*) AS Frequency\n" +
                        "FROM purchase_details PD1\n" +
                        "INNER JOIN purchase_details PD2 ON PD1.purchase_id = PD2.purchase_id\n" +
                        "INNER JOIN purchase_details PD3 ON PD1.purchase_id = PD3.purchase_id\n" +
                        "INNER JOIN product P1 ON PD1.product_id = P1.id\n" +
                        "INNER JOIN product P2 ON PD2.product_id = P2.id\n" +
                        "INNER JOIN product P3 ON PD3.product_id = P3.id\n" +
                        "INNER JOIN purchases PU ON PU.id = PD1.purchase_id\n" +
                        "WHERE PU.date >= ? AND PU.date <= ? AND " +
                        "PD1.product_id < PD2.product_id AND " +
                        "PD2.product_id < PD3.product_id\n" +
                        "GROUP BY P1.name, P2.name, P3.name\n" +
                        "ORDER BY Frequency DESC\n" +
                        "LIMIT 10;\n";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setTimestamp(1, startDate);
        preparedStatement.setTimestamp(2, endDate);

        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (resultSet.next()) {
            String frequency = resultSet.getString("Frequency");
            String productName1 = resultSet.getString("ProductName1");
            String productName2 = resultSet.getString("ProductName2");
            String productName3 = resultSet.getString("ProductName3");
            String res = "{\n\tproduct1: " + productName1 +
                    ",\n\tproduct2: " + productName2 +
                    ",\n\tproduct3: " + productName3 +
                    ",\n\tfrequency: " + frequency +
                    ",\n\tperiod: " + startDate + "-" + endDate + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task9(Connection connection, String fileName,
                             Timestamp startDate, Timestamp endDate) throws SQLException, IOException {
        String selectQuery =
                "SELECT P1.name AS ProductName1," +
                        " P2.name AS ProductName2," +
                        " P3.name AS ProductName3," +
                        " P4.name AS ProductName4," +
                        " Count(*) AS Frequency\n" +
                        "FROM purchase_details PD1\n" +
                        "INNER JOIN purchase_details PD2 ON PD1.purchase_id = PD2.purchase_id\n" +
                        "INNER JOIN purchase_details PD3 ON PD1.purchase_id = PD3.purchase_id\n" +
                        "INNER JOIN purchase_details PD4 ON PD1.purchase_id = PD4.purchase_id\n" +
                        "INNER JOIN product P1 ON PD1.product_id = P1.id\n" +
                        "INNER JOIN product P2 ON PD2.product_id = P2.id\n" +
                        "INNER JOIN product P3 ON PD3.product_id = P3.id\n" +
                        "INNER JOIN product P4 ON PD3.product_id = P4.id\n" +
                        "INNER JOIN purchases PU ON PU.id = PD1.purchase_id\n" +
                        "WHERE PU.date >= ? AND " +
                        "PU.date <= ? AND " +
                        "PD1.product_id < PD2.product_id AND " +
                        "PD2.product_id < PD3.product_id AND " +
                        "PD3.product_id < PD4.product_id\n" +
                        "GROUP BY P1.name, P2.name, P3.name, P4.name\n" +
                        "ORDER BY Frequency DESC\n" +
                        "LIMIT 10;\n";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setTimestamp(1, startDate);
        preparedStatement.setTimestamp(2, endDate);

        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (resultSet.next()) {
            String frequency = resultSet.getString("Frequency");
            String productName1 = resultSet.getString("ProductName1");
            String productName2 = resultSet.getString("ProductName2");
            String productName3 = resultSet.getString("ProductName3");
            String productName4 = resultSet.getString("ProductName4");
            String res = "{\n\tproduct1: " + productName1 +
                    ",\n\tproduct2: " + productName2 +
                    ",\n\tproduct3: " + productName3 +
                    ",\n\tproduct4: " + productName4 +
                    ",\n\tfrequency: " + frequency +
                    ",\n\tperiod: " + startDate + "-" + endDate + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task10(Connection connection, String fileName,
                              Timestamp startDate, Timestamp endDate) throws SQLException, IOException {
        String selectQuery =
                "SELECT P1.name AS ProductName1," +
                        " P2.name AS ProductName2," +
                        " P3.name AS ProductName3," +
                        " P4.name AS ProductName4," +
                        " P5.name AS ProductName5," +
                        " Count(*) AS Frequency\n" +
                        "FROM purchase_details PD1\n" +
                        "INNER JOIN purchase_details PD2 ON PD1.purchase_id = PD2.purchase_id\n" +
                        "INNER JOIN purchase_details PD3 ON PD1.purchase_id = PD3.purchase_id\n" +
                        "INNER JOIN purchase_details PD4 ON PD1.purchase_id = PD4.purchase_id\n" +
                        "INNER JOIN purchase_details PD5 ON PD1.purchase_id = PD5.purchase_id\n" +
                        "INNER JOIN product P1 ON PD1.product_id = P1.id\n" +
                        "INNER JOIN product P2 ON PD2.product_id = P2.id\n" +
                        "INNER JOIN product P3 ON PD3.product_id = P3.id\n" +
                        "INNER JOIN product P4 ON PD3.product_id = P4.id\n" +
                        "INNER JOIN product P5 ON PD3.product_id = P5.id\n" +
                        "INNER JOIN purchases PU ON PU.id = PD1.purchase_id\n" +
                        "WHERE PU.date >= ? AND " +
                        "PU.date <= ? AND " +
                        "PD1.product_id < PD2.product_id AND " +
                        "PD2.product_id < PD3.product_id AND " +
                        "PD3.product_id < PD4.product_id AND " +
                        "PD4.product_id < PD5.product_id\n" +
                        "GROUP BY P1.name, P2.name, P3.name, P4.name, P5.name\n" +
                        "ORDER BY Frequency DESC\n" +
                        "LIMIT 10;\n";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setTimestamp(1, startDate);
        preparedStatement.setTimestamp(2, endDate);

        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (resultSet.next()) {
            String frequency = resultSet.getString("Frequency");
            String productName1 = resultSet.getString("ProductName1");
            String productName2 = resultSet.getString("ProductName2");
            String productName3 = resultSet.getString("ProductName3");
            String productName4 = resultSet.getString("ProductName4");
            String productName5 = resultSet.getString("ProductName5");
            String res = "{\n\tproduct1: " + productName1 +
                    ",\n\tproduct2: " + productName2 +
                    ",\n\tproduct3: " + productName3 +
                    ",\n\tproduct4: " + productName4 +
                    ",\n\tproduct5: " + productName5 +
                    ",\n\tfrequency: " + frequency +
                    ",\n\tperiod: " + startDate + "-" + endDate + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void main(String[] args) throws SQLException {
        String url = "jdbc:ch://localhost:18123/my_database";

        Properties properties = new Properties();

        ClickHouseDataSource dataSource = new ClickHouseDataSource(url, properties);
        try (Connection connection = dataSource.getConnection("username", "password"))
        {
            addData(connection);
            System.out.println("_________________Task1___________________");
            long startTime = System.currentTimeMillis();
            task1(connection, resPath + "task1.txt");
            long endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            startTime = System.currentTimeMillis();
            task1_2(connection, resPath + "task1-2.txt", "product10");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            startTime = System.currentTimeMillis();
            task1_3(connection, resPath + "task1-3.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_________________Task2___________________");
            startTime = System.currentTimeMillis();
            task2(connection, resPath + "task2.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            startTime = System.currentTimeMillis();
            task2_1(connection, resPath + "task2-1.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            Timestamp startTS = new Timestamp(0,8,21,0,0,0,0);
            Timestamp endTS = new Timestamp(2017,8,21,0,0,0,0);

            System.out.println("_________________Task3___________________");
            startTime = System.currentTimeMillis();
            task3(connection, resPath + "task3.txt", startTS, endTS);
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            startTime = System.currentTimeMillis();
            task3_1(connection, resPath + "task3-1.txt", startTS, endTS);
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_________________Task4___________________");
            startTime = System.currentTimeMillis();
            task4(connection, resPath + "task4.txt", "product10", "shop10", startTS, endTS);
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_________________Task5___________________");
            startTime = System.currentTimeMillis();
            task5(connection, resPath + "task5.txt", "product10",startTS, endTS);
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            startTime = System.currentTimeMillis();
            task5_1(connection, resPath + "task5-1.txt", "product10",startTS, endTS);
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_________________Task6___________________");
            startTime = System.currentTimeMillis();
            task6(connection, resPath + "task6.txt", startTS, endTS);
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_________________Task7___________________");
            startTime = System.currentTimeMillis();
            task7(connection, resPath + "task7.txt", startTS, endTS);
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_________________Task8___________________");
            startTime = System.currentTimeMillis();
            task8(connection, resPath + "task8.txt", startTS, endTS);
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_________________Task9___________________");
            startTime = System.currentTimeMillis();
            task9(connection, resPath + "task9.txt", startTS, endTS);
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_________________Task10___________________");
            startTime = System.currentTimeMillis();
            task10(connection, resPath + "task10.txt", startTS, endTS);
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
